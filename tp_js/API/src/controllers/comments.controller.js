import Comment from "../models/Comment";

class CommentController {

    /**
     * Create Article into Database
     * @param {Request} req
     * @param {Response} res
     */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {

            let comment = await Comment.create({
                dateComment: req.body.dateComment,
                content: req.body.content,
                PostId: req.body.PostId,
                userId: req.body.userId,
            });

            body = {
                comment,
                'message': 'Comment created'
            };

        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let comment = await Comment.find().populate('userId postId');
            body = {
                comment,
                'message': 'Comments list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let comment = await Comment.findByID(id).populate('userId postId');

            body = {
                comment,
                'message': 'Comments details'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            await Comment.remove({_id: req.params.id});
            body = {
                'message': 'Comment delete'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            // methode 1
            let comment = await Comment.findById(req.params.id);
            Object.assign(comment, req.body);
            await comment.save();

            body = {
                comment,
                'message': 'Comments update'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
}

export default CommentController;