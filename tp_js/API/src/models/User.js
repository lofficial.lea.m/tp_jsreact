import mongoose from 'mongoose';

//Création du schema des utilisateurs et association de leur role
const userSchema = new mongoose.Schema({
    pseudo: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    roleID: {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Role'
    },

});

const User = mongoose.model('User', userSchema);
export default User;