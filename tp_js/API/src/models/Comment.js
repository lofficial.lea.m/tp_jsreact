import mongoose from 'mongoose';

//Création du schema de commentaire d'une bière
const commentSchema = new mongoose.Schema({
    dateComment: {
      type: Date,
      default: Date.now()
    },
    content: {
        type: String,
        required: true
    },
    beerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }

});

const Comment = mongoose.model('Comment', commentSchema);
export default Comment;