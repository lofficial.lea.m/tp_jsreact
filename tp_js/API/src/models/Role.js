import mongoose from 'mongoose';

//Création du schema des roles
const roleSchema = new mongoose.Schema({

    type: {
        type: String,
        required: true
    }
});

const Post = mongoose.model('Post', roleSchema);
export default Post;