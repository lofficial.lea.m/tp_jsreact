import React from 'react';
import Home from './pages/Home';
import Menu from './pages/Menu';
import Beers from './pages/Beers';
import TopBeers from './components/TopBeers';
import Detail from './pages/Detail';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';

function App() {
  return (
      <BrowserRouter>
      <Menu />
      <Route path="/" exact component={Home} />
      <Route path="/beers" exact component={Beers} />
      <Route path="/beer/:id" exact component={Detail} />
      <Route path="/topbeers" exact component={TopBeers} />

    </BrowserRouter>
  );
}

export default App;
