import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Logo from '../images/BG.jpg';

class Menu extends Component{

    render(){
        return(
            <div style={{marginBottom: "100px"}}>
                <div className="uk-position-top">
                <nav className="uk-navbar-container uk-navbar-transparent" uk-navbar="dropbar: true; dropbar-mode: push">

                    <div className="uk-navbar-left">

                        <ul className="uk-navbar-nav">
                            <li>
                                <Link to={"/"}>Home</Link>

                            </li>
                            <li>
                                <a href="#">Beers</a>
                                <div className="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                                    <div className="uk-navbar-dropdown-grid uk-child-width-1-2" uk-grid="true">
                                        <div>
                                            <ul className="uk-nav uk-navbar-dropdown-nav">
                                                <li className="uk-active"><Link to={"/beers"}>All the beers</Link></li>

                                              {/*  <li className="uk-nav-header">Header</li>
                                                <li><a href="#">Item</a></li>
                                                <li><a href="#">Item</a></li>*/}
                                                <li className="uk-nav-divider"></li>
                                                <li><Link to={"/topbeers"}>Top 10 beers</Link></li>
                                               {/* <li><a href="#">Item</a></li>*/}
                                            </ul>
                                        </div>
                                        <div>
                                            <ul className="uk-nav uk-navbar-dropdown-nav">
                                           {/*     <li className="uk-active"><a href="#">Active</a></li>
                                                <li><a href="#">Item</a></li>
                                                <li className="uk-nav-header">Header</li>
                                                <li><a href="#">Item</a></li>
                                                <li><a href="#">Item</a></li>*/}
                                                <li className="uk-nav-divider"></li>
                                                {/*<li><a href="#">Item</a></li>*/}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>

                    <div className="uk-navbar-right">

                        <ul className="uk-navbar-nav">
                            <li>
                                <a href="#">Mon compte</a>
                                <div className="uk-navbar-dropdown">
                                    <ul className="uk-nav uk-navbar-dropdown-nav">
                                        <li className="uk-active"><a href="#">Active</a></li>
                                        <li><a href="#">Item</a></li>
                                        <li className="uk-nav-header">Header</li>
                                        <li><a href="#">Item</a></li>
                                        <li><a href="#">Item</a></li>
                                        <li className="uk-nav-divider"></li>
                                        <li><a href="#">Item</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>

                    </div>

                </nav>
                </div>

                <div className="uk-navbar-dropbar"></div>
            </div>

        )
    }

}
export default Menu;