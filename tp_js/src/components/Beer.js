import React, {Component} from "react";

class Beers extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <tr>
                <td><img className="uk-border-circle" width="40" height="40" src={this.props.data.image}/></td>
                <td>{this.props.data.name}</td>
                <td>{this.props.data.description}</td>
                <td>{this.props.data.ABV}</td>
                <td>{this.props.data.EBC}</td>
                <td><button className="uk-button uk-button-danger" onClick={(id) => this.props.delete(this.props.data._id)}>Supprimer</button></td>
            </tr>
        )
    }
}
export default Beers;