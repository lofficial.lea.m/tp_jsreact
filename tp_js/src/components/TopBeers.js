import React, {Component} from "react";
import { Link } from 'react-router-dom';
import BeerService from "../services/beer.service";

class TopBeers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            beer: []
        }
    }

    async componentDidMount() {
        let response = await BeerService.listTopBeers();
        if (response.ok) {
            let data = await response.json();
            this.setState({beer: data.beer});
            console.log(this.state)

        } else {
            console.log(response.error);
        }
    }

    render() {
        return (
            <div>
            {
                this.state.beer.map((beer) => {
                    return (
                        <div>
                            <div className="uk-card uk-card-default uk-card-hover uk-width-1-2@m" key={beer._id}>
                                <div className="uk-card-header">
                                    <div className="uk-grid-small uk-flex-middle" uk-grid="true">
                                        <div className="uk-width-auto">
                                            <img className="uk-border-circle" width="40" height="40"
                                                 src={beer.image}/>
                                        </div>
                                        <div className="uk-width-expand">
                                            <h3 className="uk-card-title uk-margin-remove-bottom">"{beer.name}"</h3>
                                        </div>
                                    </div>
                                </div>
                                <div className="uk-card-body">
                                    <p>{beer.description}</p>
                                </div>
                                <div className="uk-card-footer">
                                    <Link to={'/beer/'+beer._id} className="uk-button uk-button-text">Read more</Link>
                                </div>
                            </div><br></br>
                        </div>
                    )
                })
            }
            </div>
            )
        }       
    }
    export default TopBeers;