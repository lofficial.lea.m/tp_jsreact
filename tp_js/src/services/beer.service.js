const baseUrl="http://localhost:3001";

class BeerService{
    static async list(){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };
        let call = await fetch(`${baseUrl}/beers`, init);
        return call;
    }
    static async delete(id){
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        };
        let call = await fetch(`${baseUrl}/beers/${id}`, init);
        return call;
    }
    static async listTopBeers(){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };
        let call = await fetch(`${baseUrl}/beers`, init);
        return call;
    }

    static async DetailsBeers(id){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };
        let call = await fetch(`${baseUrl}/beer/${id}`, init);
        return call;
    }
}

export default BeerService;